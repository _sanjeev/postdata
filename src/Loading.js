import React, { Component } from 'react';
import './loading.css';

class Loading extends Component {
    render() {
        return (
            <div>
                <div class="loader"></div>
            </div>
        );
    }
}

export default Loading;