import React, { Component } from "react";
import { Link } from "react-router-dom/cjs/react-router-dom.min";
import Comments from "./Comments";
import "./post.css";

class Post extends Component {
  constructor(props) {
    super(props);
    this.state = {
      userData: {},
      commRender: Array(101).fill(false),
    };
  }
  componentDidMount = () => {
    const data = this.props.data.usersData;
    const obj = data.reduce((acc, val) => {
      acc[val.id] = val.username;
      return acc;
    }, {});

    this.setState({
      userData: obj,
    });
  };

  handleClick = (event) => {};

  commArrRender(id) {
    const tempArr = this.state.commRender;
    tempArr[id] = !tempArr[id];
    this.setState({ commRender: tempArr });
  }

  render() {
    const { postsData, commentsData } = this.props.data;
    console.log(this.state);
    return (
      <div>
        {postsData.map((key) => (
          <div className="post" key={key.id}>
            <div className="post-header">
              <img
                src="https://cdn.pixabay.com/photo/2016/08/08/09/17/avatar-1577909_960_720.png"
                alt=""
              />
              <p><Link to={{pathname: `/user/${key.userId}`}}>@{this.state.userData[key.userId]}</Link></p>
            </div>
            <div className="posts">
              <p className="title">{key.title}</p>
              <p>{key.body}</p>
              <Link to={{pathname: `/post/${key.id}`}}>See Post</Link>
            </div>
            <div className="comment-icon">
              <img
                src="https://www.pngitem.com/pimgs/m/71-714713_comment-logo-png-comment-icon-transparent-png.png"
                alt="Comments"
                onClick={() => {
                  {
                    this.commArrRender(key.id);
                  }
                }}
                className="comments"
              />
              <span onClick={() => {
                  {
                    this.commArrRender(key.id);
                  }
                }}>Comments</span>
                
            </div>

            {/* <button onClick={()=>{this.commArrRender(key.id)}}>Comments:-</button> */}
            {this.state.commRender[key.id] ? (
              <Comments comData={this.props.data.commentsData} pId={key.id} />
            )
            
             : undefined}
          </div>
        ))}
      </div>
    );
  }
}

export default Post;
