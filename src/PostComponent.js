import React, { Component } from "react";
import { Link } from "react-router-dom/cjs/react-router-dom.min";
import Comments from "./Comments";
import Loading from "./Loading";
import PageNotFound from "./PageNotFound";
import "./postcomments.css";

class PostComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      postsData: [],
      usersData: [],
      commentsData: [],
      showComment: Array(11).fill(false),
      check: false,
      isLoading: false,
    };
  }

  setData = (commentData, userData, postData, id) => {
    const check = this.state.check;
    if (id > 0 && id < 101) {
      check = true;
    }
    this.setState({
      postsData: postData,
      usersData: userData,
      commentsData: commentData,
      check: check,
    });
  };

  componentDidMount = async () => {
    const id = this.props.match.params.id;
    let check = this.state.check;
    try {
      const postResponse = await fetch(
        `https://jsonplaceholder.typicode.com/posts/${id}`
        );
      const postData = await postResponse.json();
      console.log(postData.userId);
      const userResponse = await fetch(
        `https://jsonplaceholder.typicode.com/users/${postData.userId}`
      );
      const commentResponse = await fetch(
        `https://jsonplaceholder.typicode.com/posts/${id}/comments`
      );
      const commentData = await commentResponse.json();
      const userData = await userResponse.json();
      if (id > 0 && id < 101) {
        check = true;
      }
      this.setState({
        postsData: postData,
        usersData: userData,
        commentsData: commentData,
        check: check,
        isLoading: true,
      });
    } catch (error) {
      console.log(error);
    }
  };

  render() {
    const { postsData, commentsData, usersData } = this.state;
    console.log(postsData, commentsData, usersData);
    return (
      <div>
        {this.state.isLoading === false ? (
          <Loading />
        ) : (
          <div>
            {this.state.check === false ? (
              <PageNotFound />
            ) : (
              <div className="postcomments">
                <div className="post">
                  <div className="header">
                    <img
                      src="https://cdn.pixabay.com/photo/2016/08/08/09/17/avatar-1577909_960_720.png"
                      alt="Profile"
                    />
                    <h4>@{usersData.name}</h4>
                  </div>
                  <p className="para">{postsData.title}</p>
                  <p className="body">{postsData.body}</p>
                </div>
                {commentsData.map((key) => (
                  <div className="comment-data" key={key.id}>
                    <div className="commentheader">
                      <img
                        src="https://cdn.pixabay.com/photo/2016/08/08/09/17/avatar-1577909_960_720.png"
                        alt="Profile"
                      />
                      <p>@{key.name}</p>
                    </div>
                    <p className="data">{key.email}</p>
                    <p className="data">{key.body}</p>
                  </div>
                ))}
              </div>
            )}
          </div>
        )}
      </div>
    );
  }
}

export default PostComponent;
