import React, { Component } from "react";
import { Link } from "react-router-dom";
import Loading from "./Loading";
import PageNotFound from "./PageNotFound";
import "./user.css";

class Users extends Component {
  constructor(props) {
    super(props);
    this.state = {
      usersData: null,
      check: false,
      isLoading: false,
    };
  }

  setData = (userData, id) => {
    let check = this.state.check;
    let loading = this.state.isLoading;
    if (id > 0 && id < 11) {
      check = true;
    }
    this.setState({
      usersData: userData,
      check: check,
      isLoading: true,
    });
  };

  componentDidMount = async () => {
    const id = this.props.match.params.id;
    const userResponse = await fetch(
      "https://jsonplaceholder.typicode.com/users/" + this.props.match.params.id
    );
    const userData = await userResponse.json();
    this.setData(userData, id);
  };

  render() {
    const { usersData } = this.state;
    return (
      <div>
        {this.state.isLoading === false ? <Loading/> : 
        <div>
        {this.state.check === true ? (
          <div className="users">
            <img
              src="https://cdn.pixabay.com/photo/2016/08/08/09/17/avatar-1577909_960_720.png"
              alt="Profile"
              className="profile"
            />
            <div className="username">
              <h3>{usersData.name}</h3>
              <h3>(@{usersData.username})</h3>
            </div>
            <div className="item">
              <img
                src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAAAgVBMVEUAAAD///8GBgaxsbG7u7tWVlYSEhLAwMAaGhr4+Pj5+fn8/Pytra1xcXHx8fFQUFDOzs4sLCzc3Nyfn5+Dg4NfX19mZmYlJSUxMTHZ2dk1NTWNjY17e3vj4+Pr6+sqKipCQkIhISFISEicnJzIyMiAgICUlJQ9PT0QEBBbW1t0dHRM9JL6AAALIUlEQVR4nOWdZ2PiOBCGbewQmoHg0MsCy21I/v8PPIrkqjbjkWWT99Pd7QF61pamSvJ86xqvL6P4M1qcN8vuteN1rt3l5ryIPuPRZT22//Oexe8ezsLtfOWp9THfhrOBxVHYIpwFuw8NW45zN5pZGokNwlk8B8Cl+o5tUFIT9sMIRccVhdRTk5RwPJpUwnvqPCKFpCPsBxR4T02CPtm4qAin1V7OshZTopGREPbjHjHfXd2Y5EESEO53FvCe+tk3gHD9bY3vrrfKL2tFwunRKt9dXxUZKxHWwHfXsRJjBcL9Wy18d00qODtowjG1eVArQnsBWMK/tfLd9bdWwqkN+6dTDzcdMYT9hQO+uxYYFwBBGDriuyusgXCAi/2o9A1+jFDCi1O+uy52Cf9zzXfTziLh4Y9ruoeWB1uE767REkEWHADh1jVXRp8WCId0OQoKTYbUhIeTa6aCeqaT0ZBw7RpIoDUlYXPWmKzM1hsjwpFrFolGVIT1R0qm+kdD2CQrUZSB1dATfrqmUEqPqCVsNqABoo6wya/oU9tqhM1dZFJplhs1YVPNRF5qo6EkbKahL0tp+lWETXTVxFI5cArCg+txA6Rww+WEw6ZFEyr15MGUnLBZ8aBOEzhh8w1hXlLLLyNsyzKaSragSgjbtMpwSVYbCWEz0oYw/YEQNiHxC5c4VSwkdJ+6x0mY8BcRDlyPFC1R2UZE6La6VEXfZoQu64NVJTAZZcK+61FWUvk9LRO6KmHTaKEnnLoeY0WV2hlKhC66LCjV0xG2ITGjVrHvpkA4dj0+Ao2VhPW2ctlRpCKcuR4diWYKwnbF9TJN5IRttxRcUynh0fXQiHSUEb7KI8w/xCzh0fXAyHQUE77OI8w9xAxhfW3b9vUmIty7HhWp9gJCeztfXGhXJjQNfDt0unY1uvZKMkbslwhjw08a95NZkuk4Y/6BhND0b8fmpmQTDQ3HmcSJnNDYVLgm9E0Hyg0GJzQOm1pDyIMoRmieYGsNIV9rGGGAIJyG71qFNApmCMIgR2geGKaENSZWT5hnOMkSAtIzmbd0+IUYLEarAYaQJWyehIDOoNw8rCcnkAv3AISjDCFgpE9CXh+oo4jDvegZlHCSEkJm1JPwyP0++95slPwSlPC5mj4+Bqk2PQnfkgKB7Z4N3mOx8JZgwjAhhGRJOWHy8pg6ijjxDPbtB+GEUUII+ck+/0Fvw5xwm/VG1nb4WLXhhB4nBOWBU0JvyfLn9sr+bEHrP1pDEIQzRgh6zzKESQuLrSZG5jwfOo9/QxDGjBB07kOOkCfQ7WRACl+OIJwzQtDP5gn5X/O4C/oSI7EXJInrEITekxBWjikQJlNFd6YXVCc2ydMGOwzh7EFoHlfcVSRMuqzPoK/RiS/UmcFhCIMHIcwtKREmvfKU+VZePsoughjC3YMQcq6aiDDJ3NGVV4UO0x8E4cedENjjJSBMBkS1v+aHfd9P7r9iCG8umAet+4oIk5eKps+B74IpBC4owtmNEOh0CQm9r/LCgBbPdBaXLhRheCMEBgdiwsSDq949zdIr/U3xD1CE2xshMIiVEJYNNFKsR3RczlCjCOc3QqCplhJyJ6taPwfb/SJyA1GEqxshcAhywoKjjBIrigldeRThjQ/aBaUg9N7ZG7YEfidXd6x603GEYw8a+agIcwErXCuWpZas7jjCtQcNX5WEiQeHSTOe2Wdl4SqO8OJBN1GqCb3/2DDhfbhz9kmp9cIRjjxoIklDmHhw0C0b3FOTxwE4wtiD+pI6wiQH9w/0tdxTUzx7HOGnBw0ItISJBwd5/7mnppq/OMLIg04YPWHiwZl7vIHJGvyBIlx40NDcgNDrAD047qmpd63iCM9eycHVyIQw8eDMjC3z1HQ7AnGEGw/qf5gRcg/OJM3IPDWtP4sjXHrQNKAhYeLB6bpYekpPLSscYdeDusmmhNyDG6inwWZgvCrhCK/2CJOykWot456aiWXBEcIDHXNCT18o5p6akXOMI4Qzwp9hMWeWKm0gtPkMbc/Dh8TedPZkGYvz0PJayiTy7/OHrthbS+3aw/67tFDMtyW/s7jXmj206tPcRt2VeHDs/zh0jb0DrE9jwy/NZxaZUzbOnmKwYXb+6dZx78COX2ohtjgVYgtuNdJAjffIcCvBYgu1d4CNLSzEh8xNSS0AtxscOSitPybxIY4woo/xeZUmG+PzFIUf7uY/nC+X6DCI8XGEn9byNPm/uXO58bZw5Ku+jQxHGFPn2mTV0k7xtpF90RBrc204whFxvpS/bII/D3KAAh9GVNnOCkd4oc15swVjeBT94U+6VWMoXN++1G1kOMI1ad0i16Ql0N+nGRnLSsUnZRsZjnBMWXtiftjhKv/01882UiXUVG1kKxQhvH44kBIqSn/mUrSRoQg38BqwlJCoi0/eRoYinMPr+BLCHlknJnNSy3V8FOEW3oshJtSU/kCStZGhCEN4P42QUFf6g4m7sYX5gyKcwXuiRIRadwQoXoTM53dQhEN4X5uAMN0vQCVhXxuGcIXoTSwTSpq0Kkm0EQBDuEP0lw6LhLImrWoStJFhCANEj3CR0Cg8R6i8EQBDuEf0eRcITQswCBVdCAyh/yQETaA8IYmnJlPBg0MQ8l59kBXLEaqatAjE28ierjyCkO+3AE3EDCFPhdrfM/MwaAhCvmcG9KGUcEnoqcnEPbijhyHs+JwQklFMCI9sPbe7d423kX1jCNO9a5CnwAm5Tba9/zBNbcEJ0/2HkD2kT8Iz96tkpUE6JW1kVzBhuocU0kr4JOTzo459wEklHEqY2QcMadDKnW1C66nJtMmlk80/F2QIAQm3zG8dqHdzyXQ6oAiz+/EBr2n6U3WeSx8iCNlaCD4XI/2p9SioS6MpgjB/Lob5auq7lvFI2ewFn0/jjIzLdKCF82nMt7q4AktkOtDiGUPG50S5PgnLlPCU/P/8H0zdy0D0qzXK1HSXz/oyX2uUZ7AZf4vyG+UHtxl/Z/m8ttc/c+8XnJsIO1qh4cpckpD1UVwPi1BrIaF/dD0uMp19MeHrHEIrO0e4pnDPvrKP8Led5/0iRwm/+XLC17CJewXhS9yN8OOrCF//fgvLGew6FPtqQr9NN1eKdCoC/cK7glq+2EQlnt94Z1er7117L+O81t155TvJxITtvf9QdBT377zDsq33kIqvBJbksGHdfM3QSozyQvcBj8Uor3Ons8BQKAlbdy/3VgYiryW1K95/k3IoqmVtijKWcgwFYZuiYckqoyFsURJ8pqBQ1nTbsqDKllE9IeioJ3caKRk0dfk2XC1bTMzACFtgFqWG0JCw8Yg6QD1hwxG1gAaEjUbUA5oQNni5Kd5QjSVsrNFQmwkIYUNNv9LQAwkb6cCpXDU4If5sYFtaKpxtFGHT4kV5PIgnbJTVMLASCMIGrTdmawyc0D80I8m4OuiHiiSku8GiisSJXyrCBiT8hal7QkJ/4LYyNQffg4roTHe54IT64REQ+n1XhfCoXOG1Q+j7Uxe51FOpCcEioYu+G006hpzQH9vfW5lVZOqG0hH6/r4+T/WteNpbPYS36VhPy+0ZNwEpCGthrMZXmfAWG9vdxPC91g/BMuFtPtrbbbOrMP8ICW8uQGzDPvZijIEviWo/4ZTaz4kqTr9EdDsmBwGd9ZgEYAdbKtI9oeOA4rL1yQht3UWi3vXaf48qXEp2ezlDksmXkY19vfsYF0N+x4YpUJBs7VzeBztIUudjF9igu8vm3uzhLPw3150BtppvwxndulJWDbvPx+vLKP6MFufNsnvteJ1rd7k5L6LPeHRZk64pYv0PVF2UQCRTcegAAAAASUVORK5CYII="
                alt="email"
                className="user-icon"
              />
              <p>{usersData.email}</p>
            </div>
            <div className="item">
              <img
                src="https://static.xx.fbcdn.net/rsrc.php/v3/ym/r/N_tq7yNW9DG.png"
                alt="Locations"
                className="user-icon"
              />
              <p>
                {usersData.address.street}, {usersData.address.suite},{" "}
                {usersData.address.city}-({usersData.address.zipcode})
              </p>
            </div>
            <div className="item">
              <img
                src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcT7toIylCBcbCDlsBROQbfPkvLERtE0wcD0pg&usqp=CAU"
                alt="Phone"
                className="user-icon"
              />
              <p>{usersData.phone}</p>
            </div>
            <div className="item">
              <img
                src="https://cdn.pixabay.com/photo/2016/08/31/00/44/www-1632431_1280.png"
                alt="Website Link"
                className="user-icon"
              />
              <p>{usersData.website}</p>
            </div>
            <div className="item">
              <img
                src="https://static.xx.fbcdn.net/rsrc.php/v3/yk/r/M0Wls5DHC-A.png"
                alt="Company"
                className="user-icon"
              />
              <p>{usersData.company.name}</p>
            </div>
            <div className="item">
              <img
                src="https://static.xx.fbcdn.net/rsrc.php/v3/yk/r/M0Wls5DHC-A.png"
                alt="Company"
                className="user-icon"
              />
              <p>{usersData.company.catchPhrase}</p>
            </div>
            <div className="item">
              <img
                src="https://static.xx.fbcdn.net/rsrc.php/v3/yk/r/M0Wls5DHC-A.png"
                alt="Company"
                className="user-icon"
              />
              <p>{usersData.company.bs}</p>
            </div>
            {/* <p>{usersData.company.catchPhrase}</p>
            <p>{usersData.company.bs}</p> */}
          </div>
        ) : <PageNotFound />}
        </div>
        }
      </div>
    );
  }
}

export default Users;
