import React, { Component } from "react";
import "./comments.css";

class Comments extends Component {
  constructor(props) {
    super(props);
    console.log(this.props);
  }
  render() {
    return (
      <div>
        {this.props.comData.map((com) => {
          if (com.postId === this.props.pId) {
            return (
              <div className="comment-container" key={com.id}>
                <div className="comment-header">
                  <img
                    src="https://cdn.pixabay.com/photo/2016/08/08/09/17/avatar-1577909_960_720.png"
                    alt=""
                  />
                  <div>@{com.name}</div>
                </div>
                <div>{com.email}</div>
                <div>{com.body}</div>
              </div>
            );
          } else {
            return undefined;
          }
        })}
      </div>
    );
  }
}

export default Comments;
