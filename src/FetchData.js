import React, { Component } from 'react';
import Loading from './Loading';
import Post from './Post';
class FetchData extends Component {
  constructor(props) {
    super(props);
    this.state = {
      postsData: [],
      usersData: [],
      commentsData: [],
      getData: false,
    }
  }

  allDataSet = (userData, postData, commentData) => {
    const data = this.state.getData;
    this.setState({
      postsData: postData,
      usersData: userData,
      commentsData: commentData,
      getData: !data,
    })
  }

  fetchData = async() => {
    try {
      const commentResponse = await fetch('https://jsonplaceholder.typicode.com/comments');
      const commentData = await commentResponse.json();
      const userResponse = await fetch('https://jsonplaceholder.typicode.com/users');
      const userData = await userResponse.json();
      const postResponse = await fetch('https://jsonplaceholder.typicode.com/posts');
      const postData = await postResponse.json();
      this.allDataSet(userData, postData, commentData);
    } catch (error) {
      console.log(error);
    }
  }
  componentDidMount = () => {
    this.fetchData();
  }

  gotoPost = () => {

  }

  render() {
    
    return (
      <div>

      {this.state.getData === true ?
      <div> 
      <Post data={this.state}/>
      </div> : <Loading/>}

      </div>
    );
  }
}

export default FetchData;