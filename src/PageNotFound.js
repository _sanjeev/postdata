import React, { Component } from 'react';
import './error.css';

class PageNotFound extends Component {
    render() {
        return (
            <div className='error'>
                <p>Error 404: Page not found</p>
            </div>
        );
    }
}

export default PageNotFound;