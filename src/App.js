import React, { Component } from "react";
import FetchData from "./FetchData";
import {BrowserRouter as Router, Link, Route, Switch} from 'react-router-dom';
import Users from "./Users";
import PostComponent from "./PostComponent";
import Loading from "./Loading";
import PageNotFound from "./PageNotFound";

class App extends Component {
  render() {
    return (
      
      <Router>
        <Switch>
          <Route exact path='/' component={FetchData} />
          <Route exact path='/post/:id' component={PostComponent}/>
          <Route exact path='/user/:id' component={Users} />
          <Route path="*" component={PageNotFound}/>
        </Switch>

      </Router>
    );
  }
}

export default App;
